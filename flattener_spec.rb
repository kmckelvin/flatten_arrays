require_relative 'flattener'

RSpec.describe Flattener do
  describe ".flatten" do
    context "when given an empty array as an input" do
      it "returns an empty array" do
        expect(Flattener.flatten([])).to eq []
      end
    end

    context "when given a flat array as an input" do
      it "returns the same array" do
        initial = [1, 2, 3, 4]
        expected = [1, 2, 3, 4]

        expect(Flattener.flatten(initial)).to eq expected
      end
    end

    context "when given a singly nested array" do
      it "returns a new flattened array" do
        initial = [1, [2], 3, 4]
        expected = [1, 2, 3, 4]

        result = Flattener.flatten(initial)

        expect(result).to eq expected
      end

      it "does not modify the original array" do
        initial = [1, [2], 3, 4]
        result = Flattener.flatten(initial)
        expect(initial).to eq [1, [2], 3, 4]
      end
    end

    context "when given a deeply nested array" do
      it "returns a new flattened array" do
        initial = [1, [2, [3, 4, [5, 6, [7]], 8]], 9]
        expected = [1, 2, 3, 4, 5, 6, 7, 8, 9]

        result = Flattener.flatten(initial)

        expect(result).to eq expected
      end
    end

    context "given an Enumerable other than Array" do
      it "flattens Hash keys and values into a single array" do
        initial = { one: 1, two: 2 }
        expected = [:one, 1, :two, 2]

        result = Flattener.flatten(initial)

        expect(result).to eq expected
      end
    end

    context "given invalid input" do
      it "raises ArgumentError when given nil" do
        expect {
          Flattener.flatten(nil)
        }.to raise_error(ArgumentError)
      end

      it "raises ArgumentError when given an integer" do
        expect {
          Flattener.flatten(1)
        }.to raise_error(ArgumentError)
      end

      it "raises ArgumentError when given a string" do
        expect {
          Flattener.flatten("one")
        }.to raise_error(ArgumentError)
      end
    end
  end
end

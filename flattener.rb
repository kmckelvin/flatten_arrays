class Flattener
  def self.flatten(array)
    if !array.is_a?(Enumerable)
      raise ArgumentError, "Flatten must be called with an Enumerable type"
    end

    array.reduce([]) do |acc, value|
      if value.is_a?(Enumerable)
        acc += Flattener.flatten(value)
      else
        acc << value
      end
    end
  end
end

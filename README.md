### Flatten Arrays

Given an arbitrarily nested array, this demonstrates a simple recursive algorithm
to flatten the array.

Any Enumerable can be flattened - including Hashes.

This is just an exercise. In real world code please use `Array#flatten` as it's
implemented as a C function rather than in Ruby :)
